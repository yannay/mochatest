module.exports = {
	env: {
		browser: true,
		es2021: true,
		mocha: true,
		jquery: true
	},
	globals: {
		browser: true,
		driver: true
	},
	extends: [
		'airbnb-base'
	],
	parser: '@typescript-eslint/parser',
	parserOptions: {
		ecmaVersion: 12
	},
	plugins: [
		'@typescript-eslint',
		'chai-friendly'
	],
	rules: {
		'import/extensions': [
			'error',
			'ignorePackages', // ignore etensions in imports
			{
				js: 'never',
				jsx: 'never',
				ts: 'never',
				tsx: 'never'
			}
		],
		indent: [
			'error', 'tab' // indents with tabs
			// { "FunctionDeclaration": {"body": "off", "parameters": "1"} }
		],
		'no-tabs': 0, // allow tabs
		'no-console': 0, // allow console.log
		'comma-dangle': [2, 'never'], // without commas in the end of the objects
		'max-len': ['warn', { code: 120 }], // length of the line
		'no-mixed-spaces-and-tabs': ['warn'], // tabs and spaces in one line
		'class-methods-use-this': ['off'], // allow this at start of the methods
		'no-unused-vars': 'off', // ignore unused variables
		'@typescript-eslint/no-unused-vars': ['error'],	// errors for unused variables
		'no-param-reassign': 0, // allow to chenge arguments of the functions
		'no-unused-expressions': 'off', // ignore unused eexpressions
		'chai-friendly/no-unused-expressions': 'error'
	},
	settings: {
		'import/resolver': {
			node: {
				extensions: ['.js', '.jsx', '.ts', '.tsx']
			}
		}
	}
};
