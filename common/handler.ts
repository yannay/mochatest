import Logger from './logger';

class Handler {
	async okStatus(response: any) {
		if (!response.ok) {
			Logger.error(`Request error: ${response.statusText}`);
			throw Error(response.statusText);
		}
		return response;
	}
}

export default new Handler();
