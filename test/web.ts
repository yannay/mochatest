import { expect } from 'chai';
import Login from '../steps/loginStep';
import MainPage from '../pages/mainPage';
import LessonPage from '../pages/lessonPage';

const { addAttachment, startStep,	endStep } = require('@wdio/allure-reporter').default;

const screenshot = () => { addAttachment('Screenshot', Buffer.from(browser.takeScreenshot(), 'base64'), 'image/png'); };

describe('Web Suite', () => {
	beforeEach(async () => {
		Login.loginWithApi();
	});
	it('Lesson', () => {
		Login.openMainPage();

		startStep('Click lesson button');
		MainPage.lessonButton.clickWithWait();
		screenshot();
		endStep('passed');

		startStep('Click first answer button');
		LessonPage.firstAnswerButton.clickWithWait();
		endStep('passed');

		startStep('Click check answer button');
		LessonPage.checkAnswerButton.clickWithWait();
		endStep('passed');

		startStep('Click continue button');
		LessonPage.continueButton.clickWithWait();
		screenshot();
		endStep('passed');

		startStep('Click second answer button');
		LessonPage.secondAnswerButton.clickWithWait();
		endStep('passed');

		startStep('Click third answer button');
		LessonPage.thirdAnswerButton.clickWithWait();
		endStep('passed');

		startStep('Click fourth answer button');
		LessonPage.fourthAnswerButton.clickWithWait();
		endStep('passed');

		startStep('Click check answer button');
		LessonPage.checkAnswerButton.clickWithWait();
		endStep('passed');

		startStep('Click continue button');
		LessonPage.continueButton.clickWithWait();
		screenshot();
		endStep('passed');

		startStep('Lesson was completed');
		LessonPage.closeLessonButton.clickWithWait();
		screenshot();
		expect(MainPage.lessonButton.isDisplayed(), 'We are not on Main Page').to.be.true;
		expect(MainPage.successImage.isDisplayed(), "Lesson wasn't complete").to.be.true;
		endStep('passed');
	});
});
