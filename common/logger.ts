const log4js = require('log4js');

export default new class Logger {
	logger = log4js.getLogger('step');

	info(message: any) {
		this.logger.level = 'INFO';
		this.logger.info(message);
	}

	error(message: any) {
		this.logger.level = 'ERROR';
		this.logger.error(message);
	}

	shutDown() {
		log4js.shutdown();
	}
}();
