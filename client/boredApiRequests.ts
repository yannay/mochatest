import BoaredApiClient from './boredApiClient';

class BoaredApiRequests extends BoaredApiClient {
	async getRandomActivity() {
		return this.getActivity('');
	}

	async getActivityByKey(key: string) {
		return this.getActivity(`?key=${key}`);
	}

	async getActivityByType(type: string) {
		return this.getActivity(`?type=${type}`);
	}

	async getActivityByParticipants(participants: string) {
		return this.getActivity(`?participants=${participants}`);
	}

	async getActivityByPrice(price: string) {
		return this.getActivity(`?price=${price}`);
	}

	async getActivityByPriceRange(minprice: string, maxprice: string) {
		return this.getActivity(`?minprice=${minprice}&maxprice=${maxprice}`);
	}

	async getActivityByAccessibility(accessibility: string) {
		return this.getActivity(`?accessibility=${accessibility}`);
	}

	async getActivityByAccessibilityRange(minaccessibility: string, maxaccessibility: string) {
		return this.getActivity(`?minaccessibility=${minaccessibility}&maxaccessibility=${maxaccessibility}`);
	}
}

export default new BoaredApiRequests();
