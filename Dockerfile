FROM node:14.15.0

RUN cat /etc/issue

# Install tools & libs to compile everything
#RUN apt-get update && apt-get install && apt-get clean

#RUN apt install default-jre --assume-yes
#RUN apt-get install default-jdk --assume-yes
RUN apt-get update && apt-get install default-jdk -y
RUN java -version


COPY . /mochatest
WORKDIR /mochatest

RUN echo $JAVA_HOME


RUN npm install
#RUN which java
#EXPOSE 4444
#ENTRYPOINT ["sleep"]
#CMD ["99999"]