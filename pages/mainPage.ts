class MainPage {
	get lessonButton() { return $('span=You Are What You Eat'); }

	get successImage() { return $('[class*="MuiSvgIcon-frontizeLarge"]'); }
}

export default new MainPage();
