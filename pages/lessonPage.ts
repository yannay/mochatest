class LessonPage {
	get firstAnswerButton() { return $('div=True'); }

	get secondAnswerButton() { return $('div=picking the right foods'); }

	get thirdAnswerButton() { return $('div=the right balance of energy'); }

	get fourthAnswerButton() { return $('div=essential nutriens'); }

	get continueButton() { return $('span=Continue'); }

	get checkAnswerButton() { return $('span=Check Answer'); }

	get closeLessonButton() { return $('span=Close'); }
}

export default new LessonPage();
