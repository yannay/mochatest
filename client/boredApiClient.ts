import fetch from 'node-fetch';

import Config from '../config/config';
import Handler from '../common/handler';
// import Logger from '../common/logger';

class BoaredApiClient {
	// constructor() {

	// }

	async getActivity(path: string) {
		const response = await fetch(`${Config.urlApi}${path}`, {
			method: 'GET'
		}).then(Handler.okStatus);

		return response;
	}
}

export default BoaredApiClient;
