// import { expect } from 'chai';
// import Logger from '../common/logger';
import BoaredApiRequests from '../client/boredApiRequests';
import Checker from '../common/checker';
import Validate from '../client/boredApiData';
// import BoaredApiData from '../client/boredApiData';

describe('Api Suite', async () => {
	it('Random Activity', async () => {
		await BoaredApiRequests.getRandomActivity()
			.then(async (response) => {
				const json = await response.json();
				await Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});

	it('Get By Key', async () => {
		await BoaredApiRequests.getActivityByKey('5881028')
			.then(async (response) => {
				const json = await response.json();
				// await Checker.schemaValidation(json);
				// BoaredApiData.register();
				// validate('activitySchema1', json).then(errors => {
				// 	console.log(errors)
				// 	if (errors.length > 0) {
				// 	  console.log('Validation failed: ', errors);
				// 	} else {
				// 	  console.log('Validation succeed.');
				// 	}
				//   });
				await Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});

	it('Get By Key Negative', async () => {
		await BoaredApiRequests.getActivityByKey('5881027')
			.then(async (response) => {
				const json = await response.json();
				await Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeError);
			});
	});

	it('Get By Type', async () => {
		await BoaredApiRequests.getActivityByType('recreational')
			.then(async (response) => {
				const json = await response.json();
				Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});

	it('Get By Participants', async () => {
		await BoaredApiRequests.getActivityByParticipants('1')
			.then(async (response) => {
				const json = await response.json();
				Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});

	it('Get By Price', async () => {
		await BoaredApiRequests.getActivityByPrice('0.0')
			.then(async (response) => {
				const json = await response.json();
				Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});

	it('Get By Price Range', async () => {
		await BoaredApiRequests.getActivityByPriceRange('0.0', '0.5')
			.then(async (response) => {
				const json = await response.json();
				Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});

	it('Get By Accessibility', async () => {
		await BoaredApiRequests.getActivityByAccessibility('1')
			.then(async (response) => {
				const json = await response.json();
				Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});

	it('Get By Accessibility Range', async () => {
		await BoaredApiRequests.getActivityByAccessibilityRange('0.0', '0.5')
			.then(async (response) => {
				const json = await response.json();
				Checker.okStatus(response);
				Checker.schemaValidation(json, Validate.validateTypeSuccess);
			});
	});
});
