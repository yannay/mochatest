import Ajv, { JSONSchemaType } from 'ajv';

const ajv = new Ajv();

interface BoredApiDataSuccess {
	activity: string,
	type: string,
	participants: number,
	price: number,
	link: string,
	key: string,
	accessibility: number
}

interface BoredApiDataError {
	error: string,
}

const schemaSuccess: JSONSchemaType<BoredApiDataSuccess> = {
	// $async: true,
	type: 'object',
	properties: {
		activity: { type: 'string' },
		type: { type: 'string' },
		participants: { type: 'number' },
		price: { type: 'number' },
		link: { type: 'string' },
		key: { type: 'string' },
		accessibility: { type: 'number' }
	},
	required: ['activity', 'type', 'participants', 'price', 'link', 'key', 'accessibility'],
	additionalProperties: false
};

const schemaError: JSONSchemaType<BoredApiDataError> = {
	type: 'object',
	properties: {
		error: { type: 'string' }
	},
	required: ['error'],
	additionalProperties: false
};

const validateTypeSuccess = ajv.compile(schemaSuccess);
const validateTypeError = ajv.compile(schemaError);

export default { validateTypeSuccess, validateTypeError };
