import { expect } from 'chai';
import MainPage from '../pages/mainPage';

import Logger from '../common/logger';
import Config from '../config/config';

const {	startStep,	endStep } = require('@wdio/allure-reporter').default;

class LoginStep {
	loginWithApi() {
		startStep('Login with api');
		Logger.info('login to website');
		endStep('passed');
	}

	openMainPage() {
		startStep('Open main page');
		browser.url(`${Config.urlWeb}path-to-main-page`);
		expect(MainPage.lessonButton.isDisplayed(), 'We are not on Main Page').to.be.true;
		endStep('passed');
	}
}

export default new LoginStep();
