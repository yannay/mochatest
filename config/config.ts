export default new class Config {
	public urlWeb: string = 'https://beeble.com/';

	public urlApi: string = 'http://www.boredapi.com/api/activity';
}();
