import { expect } from 'chai';
import Logger from './logger';

class Checker {
	async okStatus(response: any) {
		expect(response.statusCode === 200, 'Responce code is not 200');
	}

	schemaValidation(json:any, validate:any) {
		// const v = validate(json)
		Logger.info(json);
		if (!validate(json)) {
			Logger.error(validate.errors);
		}
		expect(validate(json), 'Uncorrect response body').to.be.true;
	}
}

export default new Checker();
